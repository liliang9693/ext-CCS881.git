//% color="#4169E1" iconWidth=50 iconHeight=40
namespace DFRobot_CCS881{

    //% block="CCS811Init Time [DELAY]Cardinality [NUMBER]   " blockType="command" 
    //% DELAY.shadow="dropdown" DELAY.options="DELAY"
    //% NUMBER.shadow="number" NUMBER.defl="17531"
    export function DFRobot_CCS811Init(parameter: any, block: any) {
         let del = parameter.DELAY.code;
         let num = parameter.NUMBER.code;
     

        Generator.addInclude("DFRobot_CCS811Init", "#include <DFRobot_CCS811.h>");
        
        Generator.addObject("DFRobot_CCS811Init","DFRobot_CCS811 ",`sensor;`);
  
        Generator.addSetup(`DFRobot_CCS811InitReady`, `while(sensor.begin() != 0);`);
        //Generator.addSetup(`DFRobot_CCS811InitReady2`, ` Serial.println("failed to init chip, please check if the chip connection is fine");`);
        //Generator.addSetup(`DFRobot_CCS811InitReady3`, `delay(1000);}`)
        Generator.addSetup(`DFRobot_CCS811InitReady1`, `sensor.setMeasCycle(sensor.${del});`);
        Generator.addSetup(`DFRobot_CCS811InitReady4`, `sensor.writeBaseLine(${num});`);
        
    }
    //% block="CCS811  reading air quality data?" blockType="boolean"  
    export function DFRobot_CCS811InitReady(parameter: any, block: any) { 
        Generator.addCode( [`sensor.checkDataReady()`,Generator.ORDER_UNARY_POSTFIX]);
   
   }
    //% block="CCS811  Air[AIR] " blockType="reporter" 
    //% AIR.shadow="dropdown" AIR.options="AIR"
    export function DFRobot_CCS811InitGet(parameter: any, block: any) { 
        let air=parameter.AIR.code;
        
        Generator.addCode( [`sensor.${air}`,Generator.ORDER_UNARY_POSTFIX]);
   
   }
     //% block="CCS811Init  Print  Cardinality(Open the serial port) " blockType="command" 
  export function DFRobot_CCS811InitreadBaseLine(parameter: any, block: any) { 
    Generator.addInclude("DFRobot_CCS811Init", "#include <DFRobot_CCS811.h>");
    //Generator.addSetup(`DFRobot_CCS811Init`, "Serial.begin(115200);"); 
    Generator.addObject("DFRobot_CCS811Init","DFRobot_CCS811 ",`sensor;`);
    Generator.addSetup(`DFRobot_CCS811InitReady`, `while(sensor.begin() != 0);`);
    Generator.addCode( `Serial.println(sensor.readBaseLine(), DEC);`);

}
   
}