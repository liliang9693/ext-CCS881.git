//% color="#4169E1" iconWidth=50 iconHeight=40
namespace DFRobot_CCS881{

    //% block="CCS811 Init" blockType="command" 
    export function DFRobot_CCS811Init(parameter: any, block: any) {

        Generator.addImport("from pinpong.libs.dfrobot_ccs811 import CCS811, CCS811_Ecycle, CCS811_Emode");
        Generator.addCode("ccs811 = CCS811()");
   
    }


    //% block="CCS811  chack data?" blockType="boolean"  
    export function DFRobot_CCS811_check_data(parameter: any, block: any) { 
        Generator.addCode( [`ccs811.check_data_ready()`,Generator.ORDER_UNARY_POSTFIX]);
   
   }

    //% block="CCS811  read data [AIR] " blockType="reporter" 
    //% AIR.shadow="dropdown" AIR.options="AIR"
    export function DFRobot_CCS811InitGet(parameter: any, block: any) { 
        let air=parameter.AIR.code;
        
        Generator.addCode( [`ccs811.${air}`,Generator.ORDER_UNARY_POSTFIX]);
   
   }
    //% block="---"
    export function noteSep() {

    }

    //% block="CCS811  read base line" blockType="reporter" 
    export function DFRobot_CCS811_read_baseline(parameter: any, block: any) { 
        
        //Generator.addCode( [`ccs811.read_baseline()`,Generator.ORDER_UNARY_POSTFIX]);
        Generator.addCode( [`int(ccs811.read_baseline(),16)`,Generator.ORDER_UNARY_POSTFIX]);
   
   }
   
    //% block="CCS811 set base line [NUMBER]" blockType="command" 
    //% NUMBER.shadow="number" NUMBER.defl="17531"
    export function DFRobot_CCS811_setBase(parameter: any, block: any) {
        let num = parameter.NUMBER.code;
    
       Generator.addImport("from pinpong.libs.dfrobot_ccs811 import CCS811, CCS811_Ecycle, CCS811_Emode");
       Generator.addCode(`ccs811.write_base_line(${num})`);
  
   }

   
}