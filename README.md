# Gravity：CCS811空气质量传感器
一款低功耗、预热快、体积小的空气质量传感器，可测量CO2、TVOC，适用于空气测量应用。  

> **商品链接：** https://www.dfrobot.com.cn/goods-2581.html

![](./arduinoC/_images/featured.png)

# 积木
- **上传模式：**
![](./arduinoC/_images/blocks.png)

- **Python模式：**
![](./python/_images/blocks.png)

# 程序实例

## 为什么要获取基线？

因为之后你可以写入获取的基线，这样可以让传感器预热后立即显示空气质量，否则在污染空气中启动时需要非常久的时间才有正确读数
数据手册对基线校准的建议：在运行传感器的第一周，建议每24小时保存一个新的基线，运行1周后，可以每1-28天保存一次

**特别注意：**

- 请放在空气清新的环境中（20分钟以上）获取基线，不同传感器、不同测量周期基线不同
- 下列程序加延时，是内部获取数据有延时，如果不加延时会导致无数据。 

## 校准基线程序

- **上传模式：**

> **注意**：波特率必须设置，否则串口无数据。

![](./arduinoC/_images/example1.png)

- python模式直接打印数据即可。

![](./python/_images/example1.png)

## 读取数据程序
- **上传模式：**
![](./arduinoC/_images/example.png)

- **Python模式：**

> 固定间隔250ms

![](./python/_images/example.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|Python|备注|
|-----|-----|:-----:|-----|-----|-----|
|uno||√||||
|micro:bit||√||||
|mpython||√||||
|arduinonano||√||||
|leonardo||√||||
|mega2560||√||||
|unihiker||||√||

# 更新日志

V0.0.1 基础功能完成
V0.0.2 增加python模式支持

